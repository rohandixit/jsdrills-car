import { inventory } from "./data.js";


// Function to get cars only made by Audi or BMW
function problem6(inv = inventory){

    //  It will keep all BMW or Audi cars
    let cars = [];

    for (let index = 0; index < inv.length; index++){

        let car = inv[index];

        // If car is Audi or BMW then keep it
        if (car.car_make === 'Audi' || car.car_make === 'BMW'){
            cars.push(car)
        }
    }

    return cars;
}

export {problem6};