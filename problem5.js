import { inventory } from "./data.js";

// Function to get older cars than a year and count of cars
// made before it
function problem5(year = 2000, inv = inventory){

    // Array to store older cars
    let olderCars = [];

    for (let index = 0; index < inv.length; index++){

        // If cars year is less than year than it is older
        if (inv[index].car_year < year){
            olderCars.push(inv[index])
        };
    };

    return olderCars;
};

// Export the Function
export {problem5};