import { inventory } from "./data.js";

// Function to get last added car in inventory
function problem2(inv = inventory){
    return inv[inv.length - 1];
}

// Export function to use in test files
export {problem2};