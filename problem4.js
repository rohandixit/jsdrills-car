import { inventory } from "./data.js";

// Function to get the make year from all cars
function problem4(inv = inventory){
    
    // Array to store making year of cars
    let yearsArray = [];

    for(let index = 0; index < inv.length; index++){
        yearsArray.push(inv[index].car_year)
    };

    return yearsArray;
};

// Export the fucntion
export { problem4 };