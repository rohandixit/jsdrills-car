import { inventory } from "./data.js";

// Function to return data a given car Id
function problem1(id, inv = inventory){
    for(let index = 0; index < inv.length; index++){
        
        // Check if there is a match case with ID
        if(inv[index].id === id){
            return inv[index]
        }
    };
};

// Function Exported to use in testFiles
export {problem1};