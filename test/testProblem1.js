import { problem1 } from "../problem1.js";


// Get the car with id 33
let car = problem1(33)

// Print the Informations
console.log(`Car ${car.id} is a ${car.car_year} ${car.car_make} ${car.car_model}`)