import { problem5 } from "../problem5.js";

// get older car than 2000
let olderCars = problem5(2000);

// print the information
console.log(olderCars)
console.log(`Total cars older than year 2000 are ${olderCars.length}`)